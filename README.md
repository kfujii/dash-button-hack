# ダッシュボタン押したら何かするやつ

# 設定

`config.json.sample`を`config.json`にコピーして中身を書き換える。

`buttons`のキーはダッシュボタンのフィジカルアドレスです。`payload`はそのままSlackに投稿されます。書き方は、[Slackのドキュメント](https://api.slack.com/docs/messages)を見てください。

# 実行

```bash
$ sudo node watch.js
```
