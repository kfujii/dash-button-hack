var dashButton = require('node-dash-button');
var __ = require('underscore');
var request = require('request');
var config = require('./config.json');

var defaultUrl = config.defaultUrl;
var buttons = config.buttons;

var dash = dashButton(__.keys(buttons), null, null, 'all');

var postMessage = function(button) {
  var payload = buttons[button].payload;
  var url = defaultUrl;
  if (buttons[button].url) {
    url = buttons[button].url;
  }

  var req = request.post(url, function(err, res, body) {
    console.log(button, body);
  });

  var form = req.form();
  form.append('payload', JSON.stringify(payload));
};

dash.on('detected', function(dash_id) {
  if (buttons[dash_id]) {
    postMessage(dash_id);
  }
});
